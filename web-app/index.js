import express from 'express'
import * as couchbase from 'couchbase'

const CB_USER = process.env.CB_USER
const CB_PASS = process.env.CB_PASS
const CB_URL = process.env.CB_URL || "cb.4dzz6anchbsbxnv.cloud.couchbase.com"
const CB_BUCKET = process.env.CB_BUCKET || "default"
const IS_CAPELLA = process.env.IS_CAPELLA || true

let cb = {
  cluster: null
}

couchbase.connect('couchbases://' + CB_URL + '?tls_verify=none', {
  username: CB_USER,
  password: CB_PASS,
}).then(cluster => {

  console.log("🙂", cluster)

  cb.cluster = cluster

}).catch(err => {
  console.log("😡", err)
})




const app = express()
//const port = process.env.PORT || 3000
const port = process.env.PORT || 8080

app.use(express.static('public'))
app.use(express.json())

function fancyName() {
  let adjs = ["autumn", "hidden", "bitter", "misty", "silent", "empty", "dry",
  "dark", "summer", "icy", "delicate", "quiet", "white", "cool", "spring",
  "winter", "patient", "twilight", "dawn", "crimson", "wispy", "weathered",
  "blue", "billowing", "broken", "cold", "damp", "falling", "frosty", "green",
  "long", "late", "lingering", "bold", "little", "morning", "muddy", "old",
  "red", "rough", "still", "small", "sparkling", "throbbing", "shy",
  "wandering", "withered", "wild", "black", "young", "holy", "solitary",
  "fragrant", "aged", "snowy", "proud", "floral", "restless", "divine",
  "polished", "ancient", "purple", "lively", "nameless"]

  , nouns = ["waterfall", "river", "breeze", "moon", "rain", "wind", "sea",
  "morning", "snow", "lake", "sunset", "pine", "shadow", "leaf", "dawn",
  "glitter", "forest", "hill", "cloud", "meadow", "sun", "glade", "bird",
  "brook", "butterfly", "bush", "dew", "dust", "field", "fire", "flower",
  "firefly", "feather", "grass", "haze", "mountain", "night", "pond",
  "darkness", "snowflake", "silence", "sound", "sky", "shape", "surf",
  "thunder", "violet", "water", "wildflower", "wave", "water", "resonance",
  "sun", "wood", "dream", "cherry", "tree", "fog", "frost", "voice", "paper",
  "frog", "smoke", "star"]

  return adjs[Math.floor(Math.random()*(adjs.length-1))]+"_"+nouns[Math.floor(Math.random()*(nouns.length-1))]
}

let fancy_name = fancyName()

app.get('/api/hello', (req, res) => {
	res.send({
		message: `${process.env.GREETINGS}`,
		pod: fancy_name
	})
})

app.get('/api/coll', (req, res) => {

  cb.cluster.query('SELECT * FROM `'+CB_BUCKET+'`.data.docs').then(data => {

    console.log("🙂",data.rows)

    res.send({
      rows: data.rows
    })


  }).catch(err => {
    console.log("😡", err)
    res.send({
      error: err
    })
  })

})


app.listen(port, () => console.log(`🌍 webapp is listening on port ${port}!`))


